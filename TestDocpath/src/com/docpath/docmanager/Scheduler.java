 

package com.docpath.docmanager;

import smile.docpath.launcher.LauncherNoc;

/**
 * Created by Ruben on 1/31/14.
 */
public class Scheduler {

    //Simulamos su sheduler
    public static void main (String args[]){
        LauncherNoc launcherNoc = new LauncherNoc();

        //Sin concurrencia
        //launcherNoc.run();

        //Con concurrencia, hilos
        new Thread(launcherNoc).start();
    }
}
